package com.example.tipcalculator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private EditText salesText, tipText, totalText;
    private TextView salesTV, tipTV, totalTV, rateTV, rateMin, rateMax;
    private SeekBar tipSeekBar;
    private RadioGroup tipType;
    private RadioButton rb_no, rb_rand, rb_perc, rb_max;
    private CheckBox cb_dark;
    private View bgView;

    private int sec_black;

    protected String addTwoDoubleString(String a, String b) {
        Double aConverted, bConverted;
        double result;

        if (!a.equals("")) {
            aConverted = Double.valueOf(a).doubleValue();
        } else {
            aConverted = Double.parseDouble("0");
        }

        if (!b.equals("")) {
            bConverted = Double.valueOf(b).doubleValue();
        } else {
            bConverted = Double.parseDouble("0");
        }
        result = aConverted + bConverted;

        return String.valueOf(result);
    }

    protected String calcTip(String sales, int percent) {
        Double salesValue, tipValue;
        if (!sales.equals("")) {
            salesValue = Double.valueOf(sales).doubleValue();
            tipValue = salesValue * percent / 100;
        } else {
            tipValue = Double.parseDouble("0");
        }

        return tipValue.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        salesText = findViewById(R.id.salesText);
        tipText = findViewById(R.id.tipText);
        totalText = findViewById(R.id.totalText);
        salesTV = findViewById(R.id.salesTV);
        tipTV = findViewById(R.id.tipTV);
        totalTV = findViewById(R.id.totalTV);
        rateTV = findViewById(R.id.rateTV);
        rateMin = findViewById(R.id.rateMin);
        rateMax = findViewById(R.id.rateMax);
        tipSeekBar = findViewById(R.id.tipSeekBar);
        tipType = findViewById(R.id.tipType);
        rb_no = findViewById(R.id.rb_no);
        rb_rand = findViewById(R.id.rb_rand);
        rb_perc = findViewById(R.id.rb_perc);
        rb_max = findViewById(R.id.rb_max);
        cb_dark = findViewById(R.id.cb_dark);
        bgView = findViewById(R.id.bgView);

        sec_black = salesTV.getCurrentTextColor();

        salesText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // tip = sales * percentage
                tipText.setText(calcTip(salesText.getText().toString(), tipSeekBar.getProgress()));


                // total = sales + tip
                String added =
                        addTwoDoubleString(salesText.getText().toString(), tipText.getText().toString());
                totalText.setText(added);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tipText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String added =
                        addTwoDoubleString(salesText.getText().toString(), tipText.getText().toString());
                totalText.setText(added);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tipSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                rateTV.setText("Rate: " + String.valueOf(progress) + "%");
                tipText.setText(calcTip(salesText.getText().toString(), progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        tipType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_no:
                        tipSeekBar.setProgress(0);
                        tipSeekBar.setEnabled(false);
                        break;

                    case R.id.rb_rand:
                        break;

                    case R.id.rb_perc:
                        tipSeekBar.setEnabled(true);
                        break;

                    case R.id.rb_max:
                        tipSeekBar.setProgress(30);
                        tipSeekBar.setEnabled(false);
                        break;
                }
            }
        });

        rb_rand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipSeekBar.setEnabled(true);
                int random = new Random().nextInt(31);
                tipSeekBar.setProgress(random);
            }
        });

        cb_dark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    bgView.setBackgroundColor(Color.BLACK);
                    salesText.setTextColor(Color.WHITE);
                    tipText.setTextColor(Color.WHITE);
                    totalText.setTextColor(Color.WHITE);
                    salesTV.setTextColor(Color.WHITE);
                    tipTV.setTextColor(Color.WHITE);
                    totalTV.setTextColor(Color.WHITE);
                    rateTV.setTextColor(Color.WHITE);
                    rateMin.setTextColor(Color.WHITE);
                    rateMax.setTextColor(Color.WHITE);
                    rb_no.setTextColor(Color.WHITE);
                    rb_rand.setTextColor(Color.WHITE);
                    rb_perc.setTextColor(Color.WHITE);
                    rb_max.setTextColor(Color.WHITE);
                    cb_dark.setTextColor(Color.WHITE);
                } else {
                    bgView.setBackgroundColor(Color.WHITE);
                    salesText.setTextColor(Color.BLACK);
                    tipText.setTextColor(Color.BLACK);
                    totalText.setTextColor(Color.BLACK);
                    salesTV.setTextColor(sec_black);
                    tipTV.setTextColor(sec_black);
                    totalTV.setTextColor(sec_black);
                    rateTV.setTextColor(sec_black);
                    rateMin.setTextColor(sec_black);
                    rateMax.setTextColor(sec_black);
                    rb_no.setTextColor(Color.BLACK);
                    rb_rand.setTextColor(Color.BLACK);
                    rb_perc.setTextColor(Color.BLACK);
                    rb_max.setTextColor(Color.BLACK);
                    cb_dark.setTextColor(sec_black);
                }
            }
        });
    }
}
